package org.sfans.core.domain;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebsiteRepository extends PagingAndSortingRepository<Website, Long> {

	Page<Website> findByStatus(Website.Status status, Pageable pageable);

	Optional<Website> findByHostsAndStatus(String host, Website.Status status);

	Optional<Website> findByHostsIn(String host);
}
