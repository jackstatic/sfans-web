package org.sfans.admin.event;

import java.util.Date;

import org.sfans.core.domain.Account;
import org.sfans.core.domain.AccountRepository;
import org.sfans.core.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class SuccessfulAuthHandler implements ApplicationListener<AuthenticationSuccessEvent> {

	@Autowired
	private AccountRepository repository;

	@Override
	@Transactional
	public void onApplicationEvent(final AuthenticationSuccessEvent event) {
		final Account account = ((User) event.getAuthentication().getPrincipal()).getAccount();

		account.setFailedAuthCounter(0);
		account.setLastSuccessAuth(new Date());
		account.setLastSuccessAuthIP(surrentRequestRemoteAddr());

		repository.save(account);
	}

	private String surrentRequestRemoteAddr() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest().getRemoteAddr();
	}

}
