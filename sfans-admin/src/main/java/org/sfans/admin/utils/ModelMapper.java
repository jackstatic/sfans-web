package org.sfans.admin.utils;

public interface ModelMapper {
	<T> T from(Object source, Class<T> destinationType);

	void update(Object source, Object destination);
}
